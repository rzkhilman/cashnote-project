package com.example.chatrine.cashnoteapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.chatrine.cashnoteapp.adapter.CashNoteAdapter;
import com.example.chatrine.cashnoteapp.network.ApiServices;
import com.example.chatrine.cashnoteapp.network.InitRetrofit;
import com.example.chatrine.cashnoteapp.network.response.DataItem;
import com.example.chatrine.cashnoteapp.network.response.ResponseNote;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rvDataCatatan)
    RecyclerView rvDataCatatan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                startActivity(new Intent(MainActivity.this, AddNoteActivity.class));
            }
        });

        // eksekusi method
        getAllNotes();
    }

    public void getAllNotes(){
        // set layout manager
        rvDataCatatan.setLayoutManager(new LinearLayoutManager(this));
        ApiServices apiServices = InitRetrofit.getInstance();
        Call<ResponseNote> callRequest = apiServices.request_note();
        // kirim request
        callRequest.enqueue(new Callback<ResponseNote>() {
            @Override
            public void onResponse(Call<ResponseNote> call, Response<ResponseNote> response) {
                // pastikan response success
                if (response.isSuccessful()){
                    // tampung data ke variable
                    List<DataItem> dataBerita = response.body().getData();
                    String msg = response.body().getMsg();
                    // tampilkan toast
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();

                    // Bawa data ke adapter recycler view
                    // Panggil Adapter
                    CashNoteAdapter adapter = new CashNoteAdapter(MainActivity.this, dataBerita);
                    // Set adapter ke recycle view
                    rvDataCatatan.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseNote> call, Throwable t) {
                // cetak error di logcat
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

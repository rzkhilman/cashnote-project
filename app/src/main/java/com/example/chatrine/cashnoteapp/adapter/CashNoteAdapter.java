package com.example.chatrine.cashnoteapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chatrine.cashnoteapp.R;
import com.example.chatrine.cashnoteapp.network.response.DataItem;

import java.util.List;

public class CashNoteAdapter extends RecyclerView.Adapter<CashNoteAdapter.MyViewHolder>{
    // Komponen yang diisi dari constructor
    private Context context;
    private List<DataItem> dataBerita;

    public CashNoteAdapter(Context context, List<DataItem> dataBerita) {
        // Mengisi data ke komponen
        this.context = context;
        this.dataBerita = dataBerita;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view untuk tampilan tiap item list
        View viewList = LayoutInflater.from(context).inflate(R.layout.note_item, parent, false);
        // Buat object my holder
        MyViewHolder holder = new MyViewHolder(viewList);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // set nilai ke widget
        holder.tvTitle.setText(dataBerita.get(position).getPengeluaran());
        holder.tvDate.setText(dataBerita.get(position).getTanggal());
        holder.tvDesk.setText(dataBerita.get(position).getKeterangan());
    }

    @Override
    public int getItemCount() {
        // jumlah baris data yg ada di table
        return dataBerita.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // deklarasi variable
        TextView tvTitle, tvDate, tvDesk;
        public MyViewHolder(View itemView) {
            super(itemView);
            // inisialisasi variable
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvDesk = itemView.findViewById(R.id.tvDesk);
        }
    }
}

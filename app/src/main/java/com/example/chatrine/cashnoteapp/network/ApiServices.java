package com.example.chatrine.cashnoteapp.network;

import com.example.chatrine.cashnoteapp.network.response.ResponseNote;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiServices {
    @GET("show_cash_note.php")
    Call<ResponseNote> request_note();
}

package com.example.chatrine.cashnoteapp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditNoteActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.etPengeluaran)
    EditText etPengeluaran;
    @BindView(R.id.etKeterangan)
    EditText etKeterangan;
    @BindView(R.id.tvTanggal)
    TextView tvTanggal;
    @BindView(R.id.btnSimpan)
    Button btnSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.tvTanggal)
    public void onTvTanggalClicked() {
        // tampilkan dialog
        showDatePicker();
    }

    @OnClick(R.id.btnSimpan)
    public void onBtnSimpanClicked() {

    }

    private void showDatePicker() {
        int tahun = Calendar.getInstance().get(Calendar.YEAR);
        int bulan = Calendar.getInstance().get(Calendar.MONTH);
        int tanggal = Calendar.getInstance().get(Calendar.DATE);
        DatePickerDialog datePicker = new DatePickerDialog(this, EditNoteActivity.this, tahun, bulan, tanggal);
        datePicker.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        tvTanggal.setText(year + "-" + (month+1) + "-" + dayOfMonth);
    }
}

// Generated code from Butter Knife. Do not modify!
package com.example.chatrine.cashnoteapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddNoteActivity_ViewBinding implements Unbinder {
  private AddNoteActivity target;

  private View view2131230909;

  private View view2131230757;

  @UiThread
  public AddNoteActivity_ViewBinding(AddNoteActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddNoteActivity_ViewBinding(final AddNoteActivity target, View source) {
    this.target = target;

    View view;
    target.etPengeluaran = Utils.findRequiredViewAsType(source, R.id.etPengeluaran, "field 'etPengeluaran'", EditText.class);
    target.etKeterangan = Utils.findRequiredViewAsType(source, R.id.etKeterangan, "field 'etKeterangan'", EditText.class);
    view = Utils.findRequiredView(source, R.id.tvTanggal, "field 'tvTanggal' and method 'onTvTanggalClicked'");
    target.tvTanggal = Utils.castView(view, R.id.tvTanggal, "field 'tvTanggal'", TextView.class);
    view2131230909 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onTvTanggalClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnSimpan, "field 'btnSimpan' and method 'onBtnSimpanClicked'");
    target.btnSimpan = Utils.castView(view, R.id.btnSimpan, "field 'btnSimpan'", Button.class);
    view2131230757 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnSimpanClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddNoteActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etPengeluaran = null;
    target.etKeterangan = null;
    target.tvTanggal = null;
    target.btnSimpan = null;

    view2131230909.setOnClickListener(null);
    view2131230909 = null;
    view2131230757.setOnClickListener(null);
    view2131230757 = null;
  }
}
